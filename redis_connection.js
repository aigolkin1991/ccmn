const redis = require('redis');
const redisClient = redis.createClient();

redisClient.on('error', function (err) {
    console.log("Error occured: ". err);
});

redisClient.on('ready', function () {
    console.log('Redis is ready');
});

module.exports.redisClient = redisClient;