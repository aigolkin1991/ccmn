const request = require('request');

const test_url = 'https://cisco-presence.unit.ua/api/';
const test_login = 'RO';
const test_pass = 'Passw0rd';
const siteId = '1513804707441';

const apiUri = {
    'uniqVis': 'presence/v1/visitor/count',
    'hourlyVis': 'presence/v1/visitor/hourly',
    'averageDwell': 'presence/v1/dwell/averagebylevel',
    'hourly3days': 'presence/v1/visitor/hourly/3days',
    'kpiSummary': 'presence/v1/kpisummary',
    'dailyVisitors': 'presence/v1/visitor/daily',
    'dailyConnectedVis': 'presence/v1/connected/daily',
    'dailyRepeatVis': 'presence/v1/repeatvisitors/daily',
};

const auth = "Basic " + new Buffer.from(test_login + ":" + test_pass).toString("base64");

let today = new Date();
let month = parseInt(today.getMonth() + 1);
let day = parseInt(today.getDate());
let defaultStartDate = today.getFullYear() + "-" + 
    ((month) < 10 ? '0'+month : month) + "-" + 
    ((day) < 10 ? '0'+day : day);
let defaultEndDate = defaultStartDate;

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

function sendGetRequestPromised(requestUrl) {
    return new Promise((resolve, reject) => {
        let finalData = '';
        let req = request({
            url: requestUrl,
            timeout: 5000,
            headers: {
                "Authorization": auth
            }
        }, (e, r, b) => {
            if(e){
                resolve({
                    status: false,
                    description: e.message
                });
            }else if(r.statusCode !== 200){
                resolve({
                    status: false,
                    description: b,
                })
            }
        });
        req.on('data', (data) => {
            finalData += data;
            console.log('DATA_RECIEVED');
        });
        req.on('end', () => {
            console.log('END OF DATA');
            try {
                let obj = JSON.parse(finalData);
                resolve({
                    status: true,
                    data: obj
                });
            } catch (e) {
                console.log("RESPONSE BODY: ", finalData);
                resolve({
                    status: false,
                    description: "Invalid json response"
                });
            }
        });
        req.on('timeout', () => {
            console.log("TIMEOUT");
            resolve({
                status: false,
                description: 'timeout',
            });
        });
    });
}

function promisedDataProcessing(data, processFunc) {
    return new Promise((resolve, reject) => {
        if(data.status === false)
            resolve(data);
        else{
            let res = processFunc(data.data);
            resolve({
                status: true,
                data: res
            });
        }
    });
}

function sendResponse(responseData, responseStream){
    responseStream.write(JSON.stringify(responseData));
    responseStream.end();
}

function setUniqueVisitorsData(stD = null, enD = null, res){
    if(stD !== null)
        defaultStartDate = stD;
    if(enD !== null)
        defaultEndDate = enD;

    const completeUrl = `${test_url}${apiUri.uniqVis}?siteId=${siteId}&startDate=${defaultStartDate}&endDate=${defaultEndDate}`;

    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res);
    })
    .catch(e => {

    })
}

function setHourlyCountOfVisitors(stD = null, res){
    if(stD !== null)
        defaultStartDate = stD;
    
    let completeUrl = `${test_url}${apiUri.hourlyVis}?siteId=${siteId}&date=${defaultStartDate}`;
    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res);
    })
    .catch(e => {

    });
}

function setDwellTime(stD = null, enD = null, res){
    if(stD !== null)
        defaultStartDate = stD;
    if(enD !== null)
        defaultEndDate = enD;

    let completeUrl = `${test_url}${apiUri.averageDwell}?siteId=${siteId}&startDate=${defaultStartDate}&endDate=${defaultEndDate}`;
    
    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res)
    })
}

function hourlyVisitorsFor3Days(res){
    let completeUrl = `${test_url}${apiUri.hourly3days}?siteId=${siteId}`;
    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res);
    })
    .catch(e => {

    })
}

function kpiSummary(stD = null, enD = null, res){
    if(stD !== null)
        defaultStartDate = stD;
    if(enD !== null)
        defaultEndDate = enD;

    let completeUrl = `${test_url}${apiUri.kpiSummary}?siteId=${siteId}&startDate=${defaultStartDate}&endDate=${defaultEndDate}`;
    
    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res)
    })
    .catch(e => {

    });
}

function dailyVisitors(stD = null, enD = null, res){
    if(stD !== null)
        defaultStartDate = stD;
    if(enD !== null)
        defaultEndDate = enD;

    let completeUrl = `${test_url}${apiUri.dailyVisitors}?siteId=${siteId}&startDate=${defaultStartDate}&endDate=${defaultEndDate}`;
    
    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res);
    })
    .catch(e => {

    })
}

function dailyConnectedVisitors(stD = null, enD = null, res){
    if(stD !== null)
        defaultStartDate = stD;
    if(enD !== null)
        defaultEndDate = enD;

    let completeUrl = `${test_url}${apiUri.dailyConnectedVis}?siteId=${siteId}&startDate=${defaultStartDate}&endDate=${defaultEndDate}`;
    
    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res);
    })
    .catch(e => {

    })
}

function dailyRepeatVisitors(stD = null, enD = null, res){
    if(stD !== null)
        defaultStartDate = stD;
    if(enD !== null)
        defaultEndDate = enD;

    let completeUrl = `${test_url}${apiUri}?siteId=${siteId}&startDate=${defaultStartDate}&endDate=${defaultEndDate}`;
    sendGetRequestPromised(completeUrl)
    .then(responseObject => {
        sendResponse(responseObject, res);
    })
    .catch(e => {

    })
}

module.exports = {
    setUniqueVisitorsData, 
    setHourlyCountOfVisitors, 
    setDwellTime, 
    hourlyVisitorsFor3Days,
    kpiSummary,
    dailyVisitors,
    dailyConnectedVisitors,
    dailyRepeatVisitors
};