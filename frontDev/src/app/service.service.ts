import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  baseUrl = 'http://localhost:3000/';
  getDataAsJson(url:string, param = {}){
    let params = new HttpParams();
    for (let key in param){
      params = params.append(key,param[key]);
    }
    return this.http.get(this.baseUrl + url,{params})
  }
  getDataAsJson2(url:string, param = {}){
    let params = new HttpParams();
    for (let key in param){
      params = params.append(key,param[key]);
    }
    return this.http.get(this.baseUrl + url,{params})
  }
  getDataAsJson3(url:string, param = {}){
    let params = new HttpParams();
    for (let key in param){
      params = params.append(key,param[key]);
    }
    return this.http.get(this.baseUrl + url,{params})
  }
  getDataAsJson4(url:string, param = {}){
    let params = new HttpParams();
    for (let key in param){
      params = params.append(key,param[key]);
    }
    return this.http.get(this.baseUrl + url,{params})
  }
  constructor(private http: HttpClient) { }
}
