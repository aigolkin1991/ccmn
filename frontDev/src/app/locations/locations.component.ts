import {Component, OnInit, ViewChild, ElementRef, HostListener, Inject} from '@angular/core';
import {ServiceService} from "../service.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {delay} from "q";
import { Pipe} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from "rxjs/internal/Observable";
import {map, startWith} from "rxjs/operators";
import {MatSnackBar} from "@angular/material";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent implements OnInit {

  constructor(public service :ServiceService,public dialog: MatDialog, public snackBar: MatSnackBar) {


  }

  @ViewChild('e1') elementView: ElementRef;
  @ViewChild('e2') elementView2: ElementRef;
  @ViewChild('e3') elementView3: ElementRef;
  currentElementView : ElementRef;
  imgHeight: any;
  imgWidth: any;
  imgXcoef: any;
  imgYcoef: any;
  messages = [];
  curentCluster = 0;
  locations_data = [];
  macs = [];
  filteredMacs: Observable<string[]>;
  myControl = new FormControl();
  ngOnInit() {
    this.connect()
    this.currentElementView = this.elementView;
    this.getLocationsData();
    this.filteredMacs = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.macs.filter(option => option.toLowerCase().includes(filterValue));
  }

  getLocationsData() {
    this.service.getDataAsJson('location/location_data', {'floor': 'first'}).subscribe(res => {
      this.locations_data[0] = res;
      this.calculateImgSizes();
    });
    this.service.getDataAsJson('location/location_data', {'floor': 'second'}).subscribe(res1 => {
      this.locations_data[1] = res1;
    });
    this.service.getDataAsJson('location/location_data', {'floor': 'third'}).subscribe(res2 => {
      this.locations_data[2] = res2;
    });
    setTimeout(() =>{
      this.getMacs();
    }, 500);
    setTimeout(() => {
      this.getLocationsData()
    }, 5000);
  }


  finderSelect(location) {

    for (let i = 0; i < this.locations_data.length; i++) {
      for (let k = 0; k < this.locations_data[i].length; k++) {
        if (this.locations_data[i][k]["macAddress"] === location){
          if (this.locations_data[i][k]['mapInfo']['mapHierarchyString'] === "System Campus>UNIT.Factory>1st_Floor>1st_floor")
            this.curentCluster = 0;
          else if (this.locations_data[i][k]['mapInfo']['mapHierarchyString'] === "System Campus>UNIT.Factory>2nd_Floor>2nd_floor")
            this.curentCluster = 1;
          else
            this.curentCluster = 2;
          break;
        }
      }

    }
  }

  getMacs(){
    this.macs = [];
    for (let i = 0; i < this.locations_data.length; i ++){
      for (let k = 0; k < this.locations_data[i].length; k++){
        this.macs.push(this.locations_data[i][k]["macAddress"]);
      }
    }
  }

  calculateImgSizes(){
    this.imgHeight = this.currentElementView.nativeElement.offsetHeight;
    this.imgWidth = this.currentElementView.nativeElement.offsetWidth;
    this.imgYcoef = (this.imgHeight) / (this.locations_data[0][0]['mapInfo']['floorDimension']['length']);
    this.imgXcoef = (this.imgWidth) / (this.locations_data[0][0]['mapInfo']['floorDimension']['width']);
  }

  get_connection_style(connection){
    let style = {};
    if (connection['macAddress'] == this.myControl.value) {
      style['background-color'] = '#fbba00 ';
      style['z-index'] = '1';
    }
    style['top'] = connection['mapCoordinate']['y'] * this.imgYcoef * 0.98 + 'px';
    style['left'] = connection['mapCoordinate']['x'] * this.imgXcoef * 0.98 + 'px';
    style['height'] = this.imgWidth / 130 + 'px';
    style['width'] = this.imgWidth / 130 + 'px';
    return (style)
  }

  changeCurrentCluster(event){
    if (event === 0){
      this.curentCluster = 0;
      this.currentElementView = this.elementView;
    }
    else if (event === 1){
      this.curentCluster = 1;
      this.currentElementView = this.elementView2;
    }
    else {
      this.curentCluster = 2;
      this.currentElementView = this.elementView3;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.calculateImgSizes()
  }


  connect(): void {
    this.printMessages();
    let source = new EventSource('http://localhost:3000/location/newConnectionsEvent');
    source.addEventListener('message', message => {
     let res = JSON.parse(message['data'])['new'];
     for (let i in res)
       this.messages.push(res[i])
    });
  }

  printMessages(){
    if(this.messages[0] ){
      this.openSnackBar(this.messages[0]['mac'],this.messages[0]['floor']);
      this.messages.splice(0,1);
    }
    setTimeout(()=>{this.printMessages() }, 2200)
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message + ' is connected on ' + action,'', {
      duration: 2000,
    });
  }

  openDialog(connection : {}): void {
   this.dialog.open(popUpInformation, {
      width: 'auto',
      data: {data: connection}
    });

  }
}
export interface DialogData {
  data : {};
}

@Component({
  selector: 'popUpInformation',
  templateUrl: 'popUpInformation.html',
})
export class popUpInformation {

  constructor(
    public dialogRef: MatDialogRef<popUpInformation>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

}
