import {Component, OnInit, ɵEMPTY_ARRAY} from '@angular/core';
import {ServiceService} from "../service.service";
import { DatePipe } from '@angular/common';

export class DataRange {
  startDate: Date;
  endDate: Date;
  data: {};
  labels: string[];
  ready :boolean;
  constructor() {
    this.endDate = new Date();
    this.startDate = new Date();
    this.startDate.setDate(this.endDate.getDate() - 7);
    this.data = [{'data' : [], 'label': ''}];
    this.labels = [];
    this.ready = false;
  }
  parseData(data: {}, label){
    this.data[0]['label'] = label;
    for (let key in data){
      this.data[0]['data'].push(data[key]);
      this.labels.push(key);
    }
    this.ready = true;
  }
  updateData(data: {}){
    this.data[0]['data'] = [];
    this.labels = [];
    for (let key in data){
      this.data[0]['data'].push(data[key]);
      this.labels.push(key);
    }
  }
}


@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
  providers: [],
})


export class StatisticsComponent implements OnInit {
  connectedUsers:any = {'totalByFloor': [{},{},{},{}]};
  uniqueVisitors = new DataRange();
  datePipe = new DatePipe("en-US");
  visitorsInfo = new DataRange();
  hourlyVisitors = new DataRange();

  // Line Charts options
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';


  constructor(public service:ServiceService,public service2:ServiceService,public service3:ServiceService,public service4:ServiceService,) {}

  ngOnInit() {
    this.visitorsDatesChange();
    this.uniqueVisitorsDatesChange();
    this.hourlyVisitorsDatesChange();
    this.service.getDataAsJson('analitics/client_count').subscribe(response => {
      this.connectedUsers = response['data'];
    });

  }

 hourlyVisitorsDatesChange(event:any = null, name = null){
    if (name == 'start')
      this.hourlyVisitors.startDate = event.value;
    else if (name != null)
      this.hourlyVisitors.endDate = event.value;
    this.service2.getDataAsJson('analitics/hourly_visitors_count',
      {'startDate': this.datePipe.transform(this.hourlyVisitors.startDate,'yyyy-MM-dd')}).subscribe(respHourly => {
      if (!this.hourlyVisitors.ready)
        this.hourlyVisitors.parseData(JSON.parse(respHourly['data']), 'Hourly visitors');
      else
        this.hourlyVisitors.updateData(JSON.parse(respHourly['data']));
      console.log(respHourly)
    })
  }

  uniqueVisitorsDatesChange(event:any = null, name = null) {
    if (name == 'start')
      this.uniqueVisitors.startDate = event.value;
    else if (name != null)
      this.uniqueVisitors.endDate = event.value;
    this.service3.getDataAsJson('analitics/sum_of_unique_visitors',
      {
        'startDate': this.datePipe.transform(this.uniqueVisitors.startDate, 'yyyy-MM-dd'),
        'endDate': this.datePipe.transform(this.uniqueVisitors.endDate, 'yyyy-MM-dd')
      }).subscribe(respUnique => {
     this.uniqueVisitors.data = respUnique['data']
    })
  }

  visitorsDatesChange(event:any = null, name = null){
     if (name == 'start')
       this.visitorsInfo.startDate = event.value;
     else if (name != null)
       this.visitorsInfo.endDate = event.value;
     this.service4.getDataAsJson('analitics/visitors_count',
       {'startDate': this.datePipe.transform(this.visitorsInfo.startDate,'yyyy-MM-dd'),
         'endDate': this.datePipe.transform(this.visitorsInfo.endDate,'yyyy-MM-dd')}).subscribe(respVisitors => {
            if (!this.visitorsInfo.ready)
              this.visitorsInfo.parseData(JSON.parse(respVisitors['data']), 'Visitors per date');
            else
              this.visitorsInfo.updateData(JSON.parse(respVisitors['data']));
     })
  }



}
