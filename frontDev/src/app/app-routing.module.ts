import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatisticsComponent} from "./statistics/statistics.component";
import {LocationsComponent} from "./locations/locations.component";
const routes: Routes = [
  { path: '',              component: StatisticsComponent},
  { path: 'locations',    component: LocationsComponent },
  { path: 'statistics',   component: StatisticsComponent },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
