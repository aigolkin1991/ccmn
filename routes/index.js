const express = require('express');
const router = express.Router();
const { clientCount, clientsHistory } = require(__basedir + '/cmx_request_client_count');
const commonLocation = require(__basedir + '/cmx_location_common');

router.get('/', function(req, res, next){
    res.render('index.html');
});

router.get('/analitics', function(req, res, next){
    res.render('analitics.html');
});

router.get('/analitics/client_count', function(req, res, next){
    clientCount(res);
});

router.get('/analitics/client_history', (req, res)=>{
    clientsHistory(res);
});

router.get('/location', function(req, res, next){
    res.render('location.html');
});

router.get('/location/location_data', function(req, res, next){
    const query = req.query;
    if(query.floor){
        commonLocation.getLocationData(query.floor, res);
    }else{
        commonLocation.getLocationData(undefined, res);
    }
});

router.get('/location/newConnectionsEvent', function (req, res, next) {
    commonLocation.getNewConnectedDevices(res);
});


//*******************PRESENCE API */

const {
    setUniqueVisitorsData, 
    setHourlyCountOfVisitors, 
    setDwellTime, 
    hourlyVisitorsFor3Days,
    kpiSummary,
    dailyVisitors,
    dailyConnectedVisitors,
    dailyRepeatVisitors
} = require(__basedir+'/cmx_common_analitics');

router.get('/analitics/sum_of_unique_visitors', function(req, res, next){
    const query = req.query;
    if(query.startDate && !query.endDate){
        setUniqueVisitorsData(query.startDate, null, res);
    }else if(query.startDate && query.endDate){
        setUniqueVisitorsData(query.startDate, query.endDate, res);
    }else{
        setUniqueVisitorsData(null, null, res);
    }
});

router.get('/analitics/kpi_summary', function(req, res, next){
    const query = req.query;
    if(query.startDate && !query.endDate){
        kpiSummary(query.startDate, null, res);
    }else if(query.startDate && query.endDate){
        kpiSummary(query.startDate, query.endDate, res);
    }else{
        kpiSummary(null, null, res);
    }
});

router.get('/analitics/daily_visitors', function(req, res, next){
    const query = req.query;
    if(query.startDate && !query.endDate){
        dailyVisitors(query.startDate, null, res);
    }else if(query.startDate && query.endDate){
        dailyVisitors(query.startDate, query.endDate, res);
    }else{
        dailyVisitors(null, null, res);
    }
});

router.get('/analitics/repeat_visitors', function(req, res, next){
    const query = req.query;
    if(query.startDate && !query.endDate){
        dailyRepeatVisitors(query.startDate, null, res);
    }else if(query.startDate && query.endDate){
        dailyRepeatVisitors(query.startDate, query.endDate, res);
    }else{
        dailyRepeatVisitors(null, null, res);
    }
});

router.get('/analitics/connected_daily', function(req, res, next){
    const query = req.query;
    if(query.startDate && !query.endDate){
        dailyConnectedVisitors(query.startDate, null, res);
    }else if(query.startDate && query.endDate){
        dailyConnectedVisitors(query.startDate, query.endDate, res);
    }else{
        dailyConnectedVisitors(null, null, res);
    }
});

router.get('/analitics/hourly_visitors_count', function(req, res, next){
    const query = req.query;
    if(query.startDate){
       setHourlyCountOfVisitors(query.startDate, res);
    }else{
       setHourlyCountOfVisitors(null, res);
    }
});

router.get('/analitics/hourly_for_3_days', function(req, res, next){
    hourlyVisitorsFor3Days(res);
});

router.get('/analitics/hourly_dwell_time_by_level', function(req, res, next){
    const query = req.query;
    if(query.startDate && !query.endDate){
        setDwellTime(query.startDate, null, res);
    }else if(query.startDate && query.endDate){
        setDwellTime(query.startDate, query.endDate, res);
    }else{
        setDwellTime(null, null, res);
    }
});

module.exports = router;