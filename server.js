global.__basedir = __dirname;
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const index = require('./routes/index');
const port = 3000;

const app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(express.static(path.join(__dirname, 'client')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ecxtended: false}));

app.use('/', index);



app.listen(port, function(){
    console.log(`server started on ${port}`);
});