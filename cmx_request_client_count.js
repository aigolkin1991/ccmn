const request = require('request');
const fs = require('fs');

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const test_url = 'https://cisco-cmx.unit.ua/api/'
const test_login = 'RO';
const test_pass = 'just4reading';
const apiUri = {
    'cCount' : 'analytics/v1/now/clientCount',
    'history': 'location/v1/history/clients',
};
const auth = "Basic " + new Buffer.from(test_login + ":" + test_pass).toString("base64");

function sendGetRequestPromised(requestUrl) {
    return new Promise((resolve, reject) => {
        let finalData = '';
        let req = request({
            url: requestUrl,
            headers: {
                "Authorization": auth
            }
        }, (e, r, b) => {
            if(e){
                resolve({
                    status: false,
                    description: e.message
                });
            }else if(r.statusCode !== 200){
                resolve({
                    status: false,
                    description: b,
                })
            }
        });
        req.on('data', (data) => {
            finalData += data;
            console.log('DATA_RECIEVED');
        });
        req.on('end', () => {
            console.log('END OF DATA');
            try {
                let obj = JSON.parse(finalData);
                resolve({
                    status: true,
                    data: obj
                });
            } catch (e) {
                console.log("RESPONSE BODY: ", finalData);
                resolve({
                    status: false,
                    description: "Invalid json response"
                });
            }
        });
    });
}

function promisedDataProcessing(data, processFunc) {
    return new Promise((resolve, reject) => {
        if(data.status === false)
            resolve(data);
        else{
            let res = processFunc(data.data);
            resolve({
                status: true,
                data: res
            });
        }
    });
}

function sendResponse(responseData, responseStream){
    responseStream.write(JSON.stringify(responseData));
    responseStream.end();
}

const clientsHistory = (lastEntry) => {
    return new Promise((resolve, reject) => {
        const queryParams = `?locatedAfterTime=${lastEntry}&locatedBeforeTime=${Date.now()}`;
        const completeUrl = `${test_url}${apiUri.history}${queryParams}`;

        sendGetRequestPromised(completeUrl)
            .then((responseObject) => {
                sendResponse(responseObject, res);
            })
            .catch((e) => {

            });
    })
};

const clientCount = function (res){
    const completeUrl = `${test_url}${apiUri.cCount}`;

    sendGetRequestPromised(completeUrl)
        .then(responseObject => {
            sendResponse(responseObject, res);
        })
        .catch(e => {

        });
};

module.exports = {
    clientCount,
    clientsHistory,
};
