const EventEmitter = require('events');
const request = require('request');
const {redisClient} = require('./redis_connection');

const test_url = (process.env['DEV'] && process.env['DEV'] === '1') ? 'http://127.0.0.1/node/tests' : 'https://cisco-cmx.unit.ua/api/';
const test_login = 'RO';
const test_pass = 'just4reading';
const siteId = '1513804707441';

const floor_1_ref_id = '735495909441273878';
const floor_2_ref_id = '735495909441273979';
const floor_3_ref_id = '735495909441273980';
const floor_ids = {
    'first' : floor_1_ref_id,
    'second' : floor_2_ref_id,
    'third' : floor_3_ref_id,
};
let selected_floor = floor_1_ref_id;

let auth = "Basic " + new Buffer.from(test_login + ":" + test_pass).toString("base64");

let today = new Date();
let month = parseInt(today.getMonth() + 1);
let day = parseInt(today.getDate());
let defaultStartDate = today.getFullYear() + "-" + 
    ((month) < 10 ? '0'+month : month) + "-" + 
    ((day) < 10 ? '0'+day : day);
let defaultEndDate = defaultStartDate;

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

class MyEmitter extends EventEmitter{}
const myEmitter = new MyEmitter();

myEmitter.resultData = {
    'status': null,
    'data': null,
};

myEmitter.getLocationData = function(floor = null, res){
    const apiUri = (process.env['DEV'] && process.env['DEV'] === '1') ? '' : 'location/v1/clients';

    if(floor !== null && (floor in floor_ids)){
        selected_floor = floor_ids[floor];
    }else{
        selected_floor = floor_1_ref_id;
    }

    let completeUrl = `${test_url}${apiUri}?floorRefId=${selected_floor}&apList=${true}`;
    
    request({
        url : completeUrl,
        headers : {"Authorization" : auth}
    }).pipe(res);
};

myEmitter.getNewConnectedDevices = function (res) {
    const apiUri = (process.env['DEV'] && process.env['DEV'] === '1') ? '' : 'location/v2/clients';
    let completeUrl = `${test_url}${apiUri}`;

    request({
        url : completeUrl,
        headers : {"Authorization" : auth}
    }, function (err, response, body) {
        if (!err) {
            let bodyObj = JSON.parse(body);
            myEmitter.resultData.status = true;
            myEmitter.resultData.data = {};
            myEmitter.resultData.data.movedClients = [];
            myEmitter.resultData.data.newClients = [];
            //myEmitter.resultData.data = body;
            let promiseArr = [];
            for(let deviceObj of bodyObj){
                let tmp = {
                    'mac': deviceObj.macAddress,
                    'floor': (function () {
                        let splitedHierarchy = deviceObj.mapInfo.mapHierarchyString.split('>');
                        return splitedHierarchy[splitedHierarchy.length - 1];
                    })()
                };
                promiseArr.push(new Promise(function (resolve, reject) {
                    redisClient.exists(tmp.mac, function (err, reply) {
                        if(err){
                            console.log("Redis key check error: ", err);
                            return resolve();
                        }else {
                            if(reply === 1){
                                redisClient.get(tmp.mac, function (err, reply) {
                                    if(err){
                                        console.log('Reds key get error: ', err);
                                        return resolve();
                                    }else{
                                        if(reply !== tmp.floor){
                                            redisClient.set(tmp.mac, tmp.floor, function (err, reply) {
                                                if(err){
                                                    console.log('Redis key update (set) error: ', err);
                                                }else{
                                                    redisClient.expire(tmp.mac, 180);
                                                    if(typeof myEmitter.resultData.data.movedClients === 'undefined'){
                                                        myEmitter.resultData.data.movedClients = [];
                                                    }
                                                    myEmitter.resultData.data.movedClients.push(tmp);
                                                    return resolve();
                                                }
                                            });
                                        }else{
                                            redisClient.expire(tmp.mac, 180);
                                            return resolve();
                                        }
                                    }
                                });
                            }else{
                                redisClient.set(tmp.mac, tmp.floor, function (err, reply) {
                                    if(err){
                                        console.log('redis et key error: ', err);
                                        return resolve();
                                    }else{
                                        redisClient.expire(tmp.mac, 180);
                                        if(typeof myEmitter.resultData.data.newClients === 'undefined'){
                                            myEmitter.resultData.data.newClients = [];
                                        }
                                        myEmitter.resultData.data.newClients.push(tmp);
                                        return resolve();
                                    }
                                });
                            }
                        }
                    });
                }));
            }
            Promise.all(promiseArr).then(function (values) {
                res.writeHead(200, {
                    'Access-Control-Allow-Origin': '*',
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'text/event-stream'
                });
                res.write(`data: ${JSON.stringify({
                    'new': myEmitter.resultData.data.newClients,
                    'moved': myEmitter.resultData.data.movedClients
                })}\n\n`, function(err){
                    res.end();
                });
            });
        } else if (err) {
            console.log('Error: ' + err);
            res.write(``, function(err){
                res.end();
            });
            myEmitter.resultData.status = false;
            myEmitter.resultData.data = err;
        }
    });
};

module.exports = myEmitter;